<?php

return [
    'themes_folder' => base_path('themes'),
    'publish_assets' => true,
    'active' => 'angostura'
];
