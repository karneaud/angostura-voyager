<?php

namespace Themes\Angostura\Providers;

use Caffeinated\Themes\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        dd(theme_path('resources/views' 'angostura'));
        parent::boot();
        $this->loadViewsFrom(theme_path('resources/views' 'angostura'), 'angostura');
        //
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
