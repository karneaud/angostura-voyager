<?php

namespace App\Modules\Angostura\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
     protected $table = 'angostura_orders';
}
