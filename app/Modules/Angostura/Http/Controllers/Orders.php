<?php

namespace App\Modules\Angostura\Http\Controllers;

use Illuminate\Http\Request;

use \TCG\Voyager\Http\Controllers\VoyagerBaseController as Controller;

class Orders extends Controller
{
    public function index(Request $request)
    {
        $view = parent::index($request);
        $view->setPath(module_path( 'angostura','Resources/Views/bread/browse.blade.php'));

        return $view;
    }
}
