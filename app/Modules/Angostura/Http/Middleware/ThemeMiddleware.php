<?php

namespace App\Modules\Angostura\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Caffienated\Themes\Facades\Theme;

class ThemeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
     public function handle($request, \Closure $next, $guard = null)
     {
         if (auth()->user()->hasRole("customer")) {
             Theme::set('angostura');
             dd(Theme::getCurrent());
             // $paths = \Config::get('view.paths');
             // $base = resource_path('themes');
             // $theme = auth()->user()->theme;
             //
             // // add custom view path to the top of the path stack
             // array_unshift($paths, "$base/$theme");
             //
             // // create a new instance of the Laravel FileViewFinder and set.
             // $finder = new FileViewFinder(app()['files'], $paths);
             // app()['view']->setFinder($finder);
         }

         return $next($request);
    }
}
