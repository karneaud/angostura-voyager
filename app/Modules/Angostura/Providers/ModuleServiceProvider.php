<?php

namespace App\Modules\Angostura\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('angostura', 'Resources/Lang', 'app'), 'angostura');
        $this->loadViewsFrom(module_path('angostura', 'Resources/Views', 'app'), 'angostura');
        $this->loadMigrationsFrom(module_path('angostura', 'Database/Migrations', 'app'), 'angostura');
        $this->loadConfigsFrom(module_path('angostura', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('angostura', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
