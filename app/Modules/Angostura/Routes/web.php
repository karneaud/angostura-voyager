<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'angostura'], function () {
    Route::get('/', function () {
        dd('This is the Angostura module index page. Build something great!');
    });
});

Route::group(['as' => 'voyager.angostura-orders.index', 'middleware' => ['web','admin.user','theme.user']], function() {
    Route::get('admin/angostura-orders/','\App\Modules\Angostura\Http\Controllers\Orders@index');
});
